/**
 * Program: myls
 * Author: 	Daniel DeKerlegand
 * Date:	2 Feb. 2016
 * Course:	CSCI 2467
 */

#include "apue.h"
#include <dirent.h>
#include <unistd.h>
#include <strings.h>

static void printSort(DIR* dp, struct dirent* dirp, int size, int reverse, int hidden);
static int isFlag();
static DIR* currentdir(DIR* dp, char* buf);

int main(int argc, char *argv[]) {

	DIR* 			dp;				// pointer to directory
	struct dirent* 	dirp;			// pointer to struct to hold dir info
	char* 			buf;			// buffer for current directory name
	char			first;			// first char of file name
	int				size = 0;
	int 			flag;

	/* if argument is equal to "myls", print current dir files */

	if (argc == 1) {
		dp = currentdir(dp,buf);					// get pointer to dir
		while ( (dirp = readdir(dp)) != NULL) {		// loop through all files							
			if ((first = dirp->d_name[0] != '.'))	// don't include hidden files in size
				size++;								// increment dir size
		}
		rewinddir(dp);								// rewind dir pointer
		printSort(dp,dirp,size,0,0);
		return 0;
	} 

	/* if argument is equal to "myls -a", print current dir w/ hidden files */

	else if (argc == 2 && (flag = isFlag(argv[1])) == 1) {
		dp = currentdir(dp,buf);
		while ( (dirp = readdir(dp)) != NULL)		// loop through all files
			size++;									// increment dir size
		rewinddir(dp);								// rewind dir pointer
		printSort(dp,dirp,size,0,1);				// print out alphabetized dir
		return 0;
	} 

	/* if command is equal to "myls /dir", print dir files */

	else if (argc == 2) {
		if ((dp = opendir(argv[1])) == NULL)
			err_sys("can't open %s", argv[2]);
		while ( (dirp = readdir(dp)) != NULL) {		// loop through all files							
			if ((first = dirp->d_name[0] != '.'))	// store first char of filename
				size++;								// only count non hidden files
		}
		rewinddir(dp);
		printSort(dp,dirp,size,0,0);
		return 0;
	} 

	/* if argument is equal to "myls -a /dir", print dir w/ hidden files */

	else if (argc == 3 && (flag = isFlag(argv[1])) == 1) {
		if ((dp = opendir(argv[2])) == NULL)
			err_sys("can't open %s", argv[2]);
		while ( (dirp = readdir(dp)) != NULL)
			size++;
		rewinddir(dp);
		printSort(dp,dirp,size,0,1);
		return 0;
	} 

	/* otherwise, print an error */

	else {
		err_quit("usage:  myls directory_name");
	}
	closedir(dp);
	exit(0);
}

/* function prints out alphabetized dir */

static void printSort(DIR* dp, struct dirent* dirp, int size, int reverse, int hidden) {

	char fileList[size][1024];	// array of filenames
	char temp[1024];			// temp for swapping
	char* hidden1;				// pointer to deal with hidden file .
	char* hidden2;
	int i = 0;
	int j = 0;

	while ((dirp = readdir(dp)) != NULL) {
		if (!hidden) {								// if user did not include -a
			if (dirp->d_name[0] != '.') {			// ignore hidden files
				strcpy(fileList[i],dirp->d_name);	// store filenames in array
				i++;
			}
		} else {
			strcpy(fileList[i],dirp->d_name);		// otherwise, copy all files
			i++;
		}
	}

	/* sort array alphabetically */

	for (i=1; i < size; i++) {
		for (j=1; j < size; j++) {
			hidden1 = fileList[j-1];					// set pointer to first filename
			hidden2 = fileList[j];						// set pointer to second filename
			if (strncmp(fileList[j-1], ".", 1) == 0)	// see if it is a hidden file
				hidden1 = hidden1 + 1;						// if so, move pointer to ignore .
			if (strncmp(fileList[j], ".", 1) == 0)
				hidden2 = hidden2 + 1;
			if (strcasecmp(hidden1,hidden2)>0) {		// if strings are out of order
				strcpy(temp,fileList[j-1]);			// store first string in temp
				strcpy(fileList[j-1],fileList[j]);	// move second string up
				strcpy(fileList[j],temp);			// store temp in second string
			}
		}
	}

	for (i=0; i < size; i++)
		 printf("%s\n", fileList[i]);
}

static int isFlag(char* argument) {
	if (strcmp(argument,"-a") == 0)
		return 1;
	else if (strcmp(argument,"-ar") == 0)
		return 1;
	else if (strcmp(argument,"-ra") == 0)
		return 1;
	else if (strcmp(argument,"-r") == 0)
		return 1;
	else
		return 0;
}

static DIR* currentdir(DIR* dp, char* buf) {
	buf=(char *)malloc(100*sizeof(char));
	getcwd(buf,100);
	dp = opendir(buf);
	return dp;
}
