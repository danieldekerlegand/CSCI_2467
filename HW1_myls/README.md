DOCUMENTATION
=============

This program is a simple implementation in C of the ls utility. To compile, simply modify the CFLAGS variable in the Makefile so that it points to your local apue.3e library.