/**
 * Program: Processes
 * Author: 	Daniel DeKerlegand
 * Date:	24 Apr. 2016
 * Course:	CSCI 2467
 */

 #include <signal.h>
 #include <stdio.h>
 #include <unistd.h>
 #include <stdlib.h>

void parentStart();
void childStart();
void grandchildStart();
void parentSleep(int parentTime);
void childSleep(int childTime);
void grandchildSleep(int grandchildTime);
void parentQuit();
void childQuit();
void grandchildQuit();

int main(int argc, char *argv[]) {

	pid_t	p_id;
	int parentTime, childTime, grandchildTime;

	if (argc != 2 || (atoi(argv[1]) != 1 && 
		atoi(argv[1]) != 2 && atoi(argv[1]) != 3)) 
	{
		printf("%s","Usage: Enter 1, 2, or 3\n");
		return(0);
	}

	if (atoi(argv[1]) == 1) {
		parentTime = 10;
		childTime = 5;
		grandchildTime = 2;
	} 
	else if (atoi(argv[1]) == 2) 
	{
		parentTime = 5;
		childTime = 2;
		grandchildTime = 10;
	} 
	else if (atoi(argv[1]) == 3) 
	{
		parentTime = 2;
		childTime = 5;
		grandchildTime = 10;
	}

	parentStart();

	if ( (p_id = fork()) < 0 ) 
	{
		printf("%s","child fork error");
	}

	else if (p_id == 0) 
	{
		childStart();

		if ( (p_id = fork()) < 0 )
		{
			printf("%s","grandchild fork error");
		}
		else if (p_id == 0)
		{
			grandchildStart();
			grandchildSleep(grandchildTime);
			grandchildQuit();
		}

		childSleep(childTime);
		childQuit();
	}

	parentSleep(parentTime);
	parentQuit();
}

void parentStart()
{
	printf("Parent has pid of %d, ppid of %d, gid of %d, and session id of %d.\n", 
		getpid(), getppid(), getgid(), getsid(getpid()));
}

void childStart()
{
	printf("Child has pid of %d, ppid of %d, gid of %d, and session id of %d.\n", 
		getpid(), getppid(), getgid(), getsid(getpid()));
}

void grandchildStart()
{
	printf("Grandchild has pid of %d, ppid of %d, gid of %d, and session id of %d.\n", 
		getpid(), getppid(), getgid(), getsid(getpid()));
}

void parentSleep(int parentTime)
{
	sleep(parentTime);
}

void childSleep(int childTime)
{
	sleep(childTime);
}

void grandchildSleep(int grandchildTime)
{
	sleep(grandchildTime);
}

void parentQuit()
{
	printf("Parent exit with pid of %d, ppid of %d, gid of %d, and session id of %d.\n", 
		getpid(), getppid(), getgid(), getsid(getpid()));
	exit(0);
}

void childQuit()
{
	printf("Child exit with pid of %d, ppid of %d, gid of %d, and session id of %d.\n", 
		getpid(), getppid(), getgid(), getsid(getpid()));
	exit(0);
}

void grandchildQuit()
{
	printf("Grandchild exit with pid of %d, ppid of %d, gid of %d, and session id of %d.\n", 
		getpid(), getppid(), getgid(), getsid(getpid()));
	exit(0);
}