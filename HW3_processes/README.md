# Processes

This program simply takes three arguments: 1, 2, or 3. If 1, the grandchild process
finishes before the child and the parent. If 2, the child finishes before the parent and the
grandchild, leaving the grandchild as a zombie process. If 3, the parent finishes before the child and the grandchild, leaving both the child and grandchild as zombie processes.

To compile, simply use gcc -o processes processes.c
