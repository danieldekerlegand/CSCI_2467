#! /bin/bash

if [ $# != 1 ]; then
	echo "Usage: $0 username"
	exit 1
else
	name=$1
fi

loggedin=$(who | grep "$name" | wc -l)

echo "User $name logged in $loggedin times."