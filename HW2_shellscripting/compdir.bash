#!/bin/bash

unique=()

if [ $# != 2 ]; then
        echo "Usage: $0 dir1 dir2"
        exit 1
else
        dir1="$(ls $1)"
	dir2="$(ls $2)"
fi

unique=$(printf '%s\n%s' "$dir1" "$dir2" | sort | uniq -u)

echo "Unique filenames in $1:"

for file in $dir1; do
	for filename in $unique; do
		if [ "$file" == "$filename" ];then
			printf '%s\n' "$file"
		fi
	done
done

echo "Unique filenames in $2:"

for file in $dir2; do
	for filename in $unique; do
		if [ "$file" == "$filename" ];then
			printf '%s\n' "$file"
		fi
	done
done

